// Expression régulière de l'url de google street view
const regex = /https:\/\/www.google.fr\/maps\/@([0-9.,]+),[0-9]+a,/g;

// Fonction de récupération des éléments de l'expression régulière
function getFirstGroup(regexp, str) {
  return Array.from(str.matchAll(regexp), (m) => m[1]);
}

// Cible de la capture d'écran
const screenshotTarget = document.body;

// Connexion websocket avec le serveur
let socket = new WebSocket("ws://localhost:8080/ws");

// Evénement à l'ouverture de la connection websocket
socket.onopen = () => {
  // Envoie d'un message
  socket.send(
    // Envoie d'un handshake de type chrome-ext-hs
    JSON.stringify({
      handshake: "chrome-ext-hs",
    })
  );
  // Cacher l'interface de connexion absente
  document.getElementById("wait-conn").classList.add("hidden");
  // Afficher l'interface d'enregistrement de point
  document.getElementById("record-point").classList.remove("hidden");
};

// A la fermeture d'une connexion
socket.onclose = () => {
  // Affichage de l'interface de connexion absente
  document.getElementById("wait-conn").classList.remove("hidden");
  // Cacher l'interface d'enregistrement de point
  document.getElementById("record-point").classList.add("hidden");
};

// Lorsqu'une erreur arrive sur la connexion websocket
socket.onerror = (error) => {
  // Affichage de l'erreur dans la console
  console.log("Socket Error: ", error);
};

// Ajout d'une écoute d'événement lorsque tous les éléments sont chargés sur la page
document.addEventListener(
  "DOMContentLoaded",
  // Fonction callback de l'événement
  function () {
    // Initialisation du résultat de l'expression régulière des coordonnées
    var match = null;
    // Récupération du bouton HTML "checkPage"
    var checkPageButton = document.getElementById("checkPage");
    // Récupération de l'afficheur HTML "positionDisplay"
    var positionDisplay = document.getElementById("positionDisplay");
    // Récupération du Canvas d'affichage de la capture d'écran
    var myCanvas = document.getElementById("pictureCanvas");
    // Récupération du contexte du canvas (2d)
    var ctx = myCanvas.getContext("2d");
    // Récupération de l'élément HTML de l'affichage des erreurs
    var errorDisplay = document.getElementById("error-container");
    // Peuplement de l'affichage des erreurs avec "No error"
    errorDisplay.innerHTML = "Pas d'erreur";
    // Ajout d'un événement au clique sur le bouton d'enregistrement
    checkPageButton.addEventListener(
      "click",
      // Fonction de callback
      function () {
        // initialisation de la position
        var position = ["NA", "NA"];
        // initialisation des données d'image
        var imageData = "NA";
        // initialisation de la ville
        var ville = "NA";
        //
        try {
          // Récupération de l'onglet chrome courant
          chrome.tabs.getSelected(null, function (tab) {
            // Récupération des coordonnées en expression régulière
            match = getFirstGroup(regex, tab.url)[0];
            // Si la position est valide
            if (match && match.length > 0) {
              // séparation de la position en lat, lon
              position = match.split(",");
              // Affichage de la position dans l'élément HTML
              positionDisplay.innerHTML = position;
              // Récupération de la ville à partir de la valeur entrée par l'utilisateur dans la boîte de texte
              ville = document.getElementById("input-ville").value;
              // Capture d'écran de l'onglet visible
              chrome.tabs.captureVisibleTab(
                null,
                { format: "png" },
                // Callback de la capture
                // imageUrl: encodage de l'image
                function (imageUrl) {
                  // Création d'un objet Image
                  var image = new Image();
                  // Création d'un événement qui sera lancé lorsque l'image aura une source (src) et sera correctement chargée
                  image.onload = function () {
                    // Dessin de l'image dans le canvas
                    ctx.drawImage(image, 0, 0, 320, 180);
                    // Conversion du canvas au format BMP
                    imageData = Canvas2Image.convertToBMP(
                      // référence au canvas
                      myCanvas,
                      // Taille de l'image en pixel
                      320,
                      180
                    ).src;
                    // Envoi des informations de bitmap à la connexion websocket avec le serveur
                    socket.send(
                      // Transormation du message en JSON
                      JSON.stringify({
                        // Type: ajout d'élément dans la BDD
                        type: "add",
                        // Contenu en JSON
                        content: JSON.stringify({
                          // Latitude
                          lat: parseFloat(position[0]),
                          // Longitude
                          lon: parseFloat(position[1]),
                          // Ville
                          ville: ville,
                          // Données encodée de l'image
                          image: imageData,
                        }),
                      })
                    );
                  };
                  // Déclaration de l'image à charger et déclenchant l'événement "onload"
                  image.src = imageUrl;
                }
              );
            } else {
              // Affichage d'une erreur dans l'expression régulière
              // Cette erreur apparaît particulièrement quand l'URL est en google.com ou tout autre .pays
              errorDisplay.innerHTML = "Pas de correspondance. Vérifier l'url (google.fr)";
            }
          });
        } catch (err) {
          // Affichage de tout autre erreur
          errorDisplay.innerHTML = err.toString();
        }
      },
      false
    );
  },
  false
);
