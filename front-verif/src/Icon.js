import L from 'leaflet';

// Définition de l'icône du marqueur pour les points utilisateurs
var userIcon = new L.Icon({
    iconUrl: require('./marker-icon-blue.png'),
    iconRetinaUrl: require('./marker-icon-blue.png'),
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

// Définition de l'icône du marqueur pour les points OSM
var osmIcon = new L.Icon({
    iconUrl: require('./marker-icon-orange.png'),
    iconRetinaUrl: require('./marker-icon-orange.png'),
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

export { userIcon, osmIcon};