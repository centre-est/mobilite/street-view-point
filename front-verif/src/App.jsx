/**
 * Script React principal de l'application
 * auteur : Aurélien Clairais - Cerema Centre-Est
 */

// Imports React
import React, { useEffect, useState } from "react";
import { MapContainer, TileLayer, GeoJSON } from "react-leaflet";

// Import du module permettant d'utiliser React avec un protocole websocket
import useWebSocket, { ReadyState } from "react-use-websocket";

// Import du composant marqueur AppMarker pour l'affichage des points sur la carte
import AppMarker from "./AppMarker";

// Import des éléments Material UI
import {
  Button,
  Typography,
  FormControlLabel,
  Switch,
  Stack,
} from "@mui/material";
// Import de l'icon google via la bibliothèque de Material UI
import GoogleIcon from "@mui/icons-material/Google";

// Import du contour du département
import { dept_contour } from "./contour.js";

// Import de la feuille de style de l'application
import "./App.css";

/**
 * Composant React de l'application principale sous forme de fonction
 *
 * @returns Rendu JSX de l'application principale
 */
const App = () => {
  // Définition de l'état du composant : socketUrl = url websocket de communication avec le serveur
  const [
    socketUrl, // variable (pas de fonction de définition, si besoin utiliser setSocketUrl)
  ] = useState("ws://localhost:8080/ws");
  // Définition du hook d'utilisation du websocket
  const {
    sendMessage, // Récupération de la fonction sendMessage
    lastMessage, // récupération de l'objet lastMessage
    readyState, // récupértion de l'objet readyState
  } = useWebSocket(socketUrl);
  // Définition de l'état du composant : records = liste de tous les points enregistrés
  const [
    records, //variable
    setRecords, // fonction de définition
  ] = useState([]);
  // Définition de l'état du composant : map = objet représentant la carte
  const [
    map, // variable
    setMap, // fonction de définition
  ] = useState(null);
  // Définition de l'état du composant : inWindow = booléen représentant le choix d'utilisateur d'ouvrir Google Maps dans un onglet directement
  const [inWindow, setInWindow] = useState(false);

  // Récupération de l'état de la connexion
  const connexionStatus = {
    [ReadyState.CONNECTING]: "Connecting",
    [ReadyState.OPEN]: "Open",
    [ReadyState.CLOSING]: "Closing",
    [ReadyState.CLOSED]: "Closed",
    [ReadyState.UNINSTANTIATED]: "Uninstantiated",
  }[readyState];

  // Position du centre de la carte au départ
  const position = [45.8828, 4.4261];

  // Hook peremettant l'envoi d'un handshake à la première connexion
  useEffect(() => {
    sendMessage(
      JSON.stringify({
        handshake: "admin-hs",
      })
    );
  }, []);

  // Hook permettant la récupération de tous les points à partir du dernier message envoyé par le serveur
  // On peut faire ça puisque le serveur ne communique à l'interface admin un seul message : la liste de tous les points
  useEffect(() => {
    if (lastMessage !== null) {
      setRecords(JSON.parse(lastMessage.data));
    }
  },
  [lastMessage] // Le hook s'active lorsqu'il y a un changement sur la variable lastMessage (i.e. on reçoit un message du serveur)
  );

  /**
   * Fonction permettant l'envoi d'une requête de suppression d'un point au serveur
   */
  const askForDelete = (id) => {
    return sendMessage(
      JSON.stringify({
        type: "delete",
        content: JSON.stringify({
          idRecord: id,
        }),
      })
    );
  };

  /**
   * Fonction de création de l'ensemble des marqueurs à afficher sur la carte
   * 
   * @returns Une liste d'éléments JSX : l'ensemble des AppMarker
   */
  const recordsMarkerJSX = () => {
    return records.map((record) => {
      return (
        <AppMarker
          key={`pt_${record.idRecord}`}
          askForDelete={askForDelete}
          {...record}
        />
      );
    });
  };

  /**
   * Fonction callback du clique sur le bouton Google Maps
   * 
   * @param {MouseEvent} e : événement du clique sur le bouton
   */
  const gmapsClick = (e) => {
    // Si la carte n'existe pas, alors on ne fait rien
    if (!map) return;
    // Récupération du centre de la carte
    var center = map.getCenter();
    // Url complet de la fenetre sur Google Maps
    var link = `https://www.google.fr/maps/@${center.lat},${
      center.lng
    },${map.getZoom()}z`;
    // Si l'utilisateur veut ouvrir directement dans un nouvel onglet 
    if (inWindow) {
      window.open(link, "_blank").focus();
    } else { // sinon copier l'url dans le presse-papier
      navigator.clipboard.writeText(link);
    }
  };

  /**
   * Fonction de création du composant JSX de la carte
   * 
   * @returns Element JSX de la carte 
   */
  const mapApp = () => {
    return (
      <div>
        {/* Stack de légende des marqueurs */}
        <Stack direction="row" spacing={2} style={{ marginTop: "10px" }}>
          <img src={require("./marker-icon-blue.png")} />
          <Typography variant="body1">Utilisateur</Typography>
          <img src={require("./marker-icon-orange.png")} />
          <Typography variant="body1">OSM</Typography>
        </Stack>
        {/* Carte */}
        <MapContainer
          id="the_map"
          center={position}
          whenCreated={setMap}
          zoom={10}
          style={{ height: "650px" }}
        >
          {/* Couche de fond de carte à partir de open street map */}
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {/* Couche du contour du département */}
          <GeoJSON data={JSON.parse(dept_contour)} />
          {/* Appel de la fonction de création des marqueurs sur la carte */}
          {recordsMarkerJSX()}
        </MapContainer>
        {/* Bouton "ouvrir dans google maps" */}
        <Stack>
          <FormControlLabel
            control={
              <Switch
                checked={inWindow}
                onChange={(e) => setInWindow(e.target.checked)} // appel de la fonction de changement de l'état inWindow
                name="clipboardswitch"
              />
            }
            label="Ouvrir dans une nouvelle fenêtre"
          />
          <Button
            variant="contained"
            onClick={(e) => {
              gmapsClick(e); // appel de la fonction callback du clique sur le bouton
            }}
          >
            <GoogleIcon style={{ marginRight: "20px" }} /> Google Maps
          </Button>
        </Stack>
      </div>
    );
  };

  // Création des éléments
  return (
    <div>
      {connexionStatus === "Open" // Si la connexion est ouverte
        ? mapApp() // Afficher la carte
        : "Pas de connexion au serveur..." // Sinon afficher qu'il n'y a pas de connexion
        }
    </div>
  );
};

export default App;
