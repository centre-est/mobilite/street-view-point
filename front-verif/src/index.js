import React from "react";
import ReactDOM from "react-dom";
// Import de l'application App
import App from './App'

// Rendu de l'appli App sur le DOM dans le <div> avec id="root"
ReactDOM.render(<App />, document.querySelector("#root"))