/**
 * Script React des marqueurs des points sur la carte
 * auteur : Aurélien Clairais - Cerema Centre-Est
 */

// Import des dépendances React, Leaflet
import React, { useState } from "react";
import { Marker, Popup, Tooltip } from "react-leaflet";
import { confirm } from "react-confirm-box";
// Import des dépendances Material UI
import LocationOnIcon from "@mui/icons-material/LocationOn";
import LocationCityIcon from "@mui/icons-material/LocationCity";
import DeleteIcon from "@mui/icons-material/Delete";
import PanoramaIcon from "@mui/icons-material/Panorama";
import {
  Button,
  IconButton,
  FormControlLabel,
  Checkbox,
  Typography,
  Stack,
} from "@mui/material";

// Import des Icons définis dans Icon.js
import { osmIcon, userIcon } from "./Icon";

/**
 * Composant React d'un marqueur
 *
 * @returns Rendu JSX du marqueur
 */
const AppMarker = (props) => {
  // Définition de l'état de la source de l'image encodé en chaîne de caractère 64bits
  const [
    imageSrc, // variable
    setImageSrc, // fonction de définition
  ] = useState(""); // initialisation à ""

  /**
   * Fonction de callback du bouton de suppression
   *
   * @param {MouseEvent} e : Evenement de clique sur le bouton
   */
  const onDeleteClick = async (e) => {
    // Appel de la boîte de dialogue de confirmation
    // résultat (bool) dans la variable result
    const result = await confirm(
      "Etes-vous sûr(e) ?", // Texte de la boite
     {
      // Texte des boutons de la boîte
      labels: {
        confirmable: "Oui", // Oui pour le bouton qui confirme
        cancellable: "Non", // Non pour le bouton qui annule
      },
    });
    // Si c'est confirmé
    if (result) {
      // On appel la fonction askForDelete passé en propriété dans l'objet
      // Cette fonction doit renvoyer vers askForDelete de l'application principale
      props.askForDelete(props.idRecord);
    }
  };

  /**
   * Fonction de callback d'un clique sur un marqueur
   *
   * @param {MouseEvent} e : Evenement de clique sur le bouton
   */
  const markerClick = (e) => {
    var link = `https://www.google.fr/maps/@${props.lat},${props.lon},21z`;
    navigator.clipboard.writeText(link);
  };

  // renvoie du rendu JSX du marqueur
  return (
    // Composant Marqueur leaflet
    <Marker
      position={[props.lat, props.lon]} // position du marqueur à partir des propriété entrée dans l'appli principale
      icon={props.image ? userIcon : osmIcon} // icon Utilisateur si image et OSM si pas d'image
    >
      {/* Fenetre contextuelle au passsage de la souris sur le marqueur */}
      <Tooltip>
        {/* Affichage des coordonnées */}
        <b>
          {props.lat},{props.lon}
        </b>{" "}
        <br />
        {/* Affichage de la ville */}
        <b>Ville: {props.ville}</b>
      </Tooltip>
      {/* Popup affichée au clique sur un marqueur */}
      <Popup closeOnEscapeKey={true} maxWidth={"auto"}>
        <Stack spacing={1}>
          <Stack direction="row" spacing={2}>
            {/* Affichage de la position avec l'icone de position*/}
            <LocationOnIcon style={{ cursor: "pointer" }} onClick={markerClick} />
            <Typography variant="h6">
              {props.lat},{props.lon}
            </Typography>
          </Stack>
          {/* Affichage de la ville avec l'icone de ville */}
          <Stack direction="row" spacing={2}>
            <LocationCityIcon />
            <Typography variant="h6">{props.ville}</Typography>
          </Stack>
          {/* Affichage de la source */}
          <Typography variant="p">
            <i>Source {props.image ? "utilisateur" : "OSM"}</i>
          </Typography>
          {/* Bouton supprimer  */}
          <Button
            aria-label="delete"
            variant="outlined"
            color="error"
            onClick={onDeleteClick} // Appel du callback du bouton suppression
          >
            <DeleteIcon />
            Supprimer
          </Button>
          {/* Affichage de l'image */}
          {props.image ? (
            <img src={props.image} alt="image" width={330} height={180} />
          ) : null}
        </Stack>
      </Popup>
    </Marker>
  );
};

// Export du composant pour import dans l'application principale
export default AppMarker;
