module github.com/AureClai/SimpleSVServer

go 1.17

require (
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.11 // indirect
)
