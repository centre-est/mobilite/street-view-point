package main

import (
	"fmt"
	"log"
	"net/http"
)

// Fonction point d'entrée du programme
func main() {
	// Création de la BDD (création du fichier ou connexion au fichier)
	database := CreateSQLite3DB()
	// Libération de la mémoire RAM lorsque le programme s'arrête
	defer database.Close()
	// Création de l'objet ensemblier
	wrapper := NewWrapper(nil, nil, database)
	// Gestion du point d'entrée "localhost" avec une requête de type ws (websocket)
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) { wsEndpoint(w, r, wrapper) })
	fmt.Println("Serveur démarré, en attente de points.")
	// Ecoute du port 8080
	log.Fatal(http.ListenAndServe(":8080", nil))
}
