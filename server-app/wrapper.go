package main

import (
	"database/sql"
	"encoding/json"
	"log"

	"github.com/gorilla/websocket"
)

// Structure représentant l'assemblage des éléments de l'application
type Wrapper struct {
	// Référence à l'objet client websocket reservé à l'extension Chrome
	ChromeExt *websocket.Conn
	// Référence à l'objet client websocket reservé à l'interface administrateur
	AdminApp *websocket.Conn
	// Référence à l'objet base de données des points
	Database *sql.DB
	// Objet channel permettant les communications asynchrones entre les objets sous forme de message websocket
	Communications chan Communication
}

// Structure représentant un élément de communication composé d'un emetteur et d'un message
type Communication struct {
	// Emetteur du message sous la forme d'une référence à sa connection websocket
	Origin *websocket.Conn
	// Tableaux de bits (binaires) représentant le message
	Message []byte
}

// Structure Message représentant la version décodée d'un message
type Message struct {
	// Type du message en format texte
	Type string `json:"type"`
	// Contenu du message en format texte
	Content string `json:"content"`
}

// Structure d'un message de suppression de point
type DeleteMessage struct {
	// Identifiant numérique de l'enregistrement à supprimer
	Id int `json:"idRecord"`
}

// Fonction qui crée un nouveau Wrapper
//
// Arguments:
//
//	ChromeExt: 	Référence à l'objet client websocket reservé à l'extension Chrome.
//	AdminApp: 	Référence à l'objet client websocket reservé à l'interface administrateur
//	database: 	Référence à l'objet base de donnée.
//
// Valeur de retour:
//
//	Référence à un objet Wrapper
func NewWrapper(ChromeExt *websocket.Conn, AdminApp *websocket.Conn, database *sql.DB) *Wrapper {
	// Création d'un nouveau objet Wrapper et récupération de la référence
	wrapper := &Wrapper{
		ChromeExt:      ChromeExt,
		AdminApp:       AdminApp,
		Database:       database,
		Communications: make(chan Communication),
	}
	// Lancement de l'écoute des communications
	wrapper.run()
	//
	return wrapper
}

// Fonction du Wrapper permettant l'écoute et la gestion des messages sur le channel.
//
// Asynchrone - Goroutine
func (wrapper *Wrapper) run() {
	go func() {
		for Communication := range wrapper.Communications {
			wrapper.handleCommunication(Communication)
		}
	}()
}

// Fonction permettant l'envoie de tout les points à l'interface administrateur.
func (wrapper *Wrapper) sendAllPointsToAdmin() {
	// Test si une application administrateur est connectée
	if wrapper.AdminApp != nil { // Oui
		// Récupération de l'ensemble des entrées depuis la base de donnée
		records := getAllRecords(wrapper.Database)
		// S'il y a au moins une entrée
		if len(records) != 0 { // Oui
			// Encodage de l'ensemble des entrées de la BDD au format JSON
			message, err := json.Marshal(records)
			if err != nil {
				log.Panicln(err)
			}

			// Envoi d'un message websocket à l'interface admin contenant l'ensemble des enregistrements au format JSON
			err = wrapper.AdminApp.WriteMessage(1, message)
			if err != nil {
				log.Panicln(err)
			}
		}
	}
}

// Fonction gérant une communication (origine + message)
//
// Arguments:
//
//	communication: 	Objet Communication à gérer
func (wrapper *Wrapper) handleCommunication(communication Communication) {
	// Création d'une référence vide à un objet Message
	message := new(Message)
	// Transformation du message au format texte json au format objet Message
	if err := json.Unmarshal(communication.Message, message); err != nil {
		log.Panicln(err.Error())
	}

	// Si l'emetteur est l'extension Chrome
	if communication.Origin == wrapper.ChromeExt {
		// Agir selon le type de message
		switch message.Type {
		// Ordre d'ajouter une entrée
		case "add":
			// Création d'une nouvelle entrée vide
			record := new(Record)
			// Transformation des informations de l'entrée de JSON au format Record
			if err := json.Unmarshal([]byte(message.Content), record); err != nil {
				log.Panicln(err.Error())
			}
			// Appel de la fonction d'enregistrement d'une entrée dans la base de donnée
			insertRecord(wrapper.Database, record)
			// Envoie de la liste des points complète à l'interface administrateur pour mise à jour GUI
			wrapper.sendAllPointsToAdmin()
		}
	}

	// Si l'emetteur est l'interface administrateur
	if communication.Origin == wrapper.AdminApp {
		// Agir selon le type de message
		switch message.Type {
		// Ordre de supprimer une entrée
		case "delete":
			// Création d'une référence vide à un objet DeleteMessage
			deleteMessage := new(DeleteMessage)
			// Transformation de l'information JSON en objet DeleteMessage
			if err := json.Unmarshal([]byte(message.Content), deleteMessage); err != nil {
				log.Panicln(err.Error())
			}
			// Appel de la fonction de suppression d'une entrée dans la BDD
			deleteRecord(wrapper.Database, deleteMessage.Id)
			// Envoie de la liste des points complète à l'interface administrateur pour mise à jour GUI
			wrapper.sendAllPointsToAdmin()
		}
	}
}
