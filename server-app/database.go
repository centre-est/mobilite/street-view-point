package main

import (
	"database/sql"
	"errors"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

// Structure représentant une entrée de la base de donnée dans le contexte du serveur
type Record struct {
	// Identifiant de l'entrée
	// Nombre entier
	Id int `json:"idRecord"`
	// Latitude de l'entrée
	// Nombre décimal
	Lat float64 `json:"lat"`
	// Longitude de l'entrée
	// Nombre décimal
	Lon float64 `json:"lon"`
	// Ville de l'entrée
	// Texte
	Ville string `json:"ville"`
	// Image associée
	// Texte (encodage du bitmap au format 64bits)
	Image string `json:"image"`
}

// Fonction créeant l'objet base de donnée dans le serveur
//
// Valeurs de retour:
//
//	Référence à un objet du type DB du module sql représentant la connection au fichier sqlite physique de la BDD
func CreateSQLite3DB() *sql.DB {
	// Déclaration de la variable du fichier
	var file *os.File
	// Déclaration de la variable d'erreur
	var err error
	// Si le fichier de base de donnée existe
	// Le chemin est ../database/sqlite-database.db depuis l'emplacement ou l'appli est lancée
	if _, err = os.Stat("../database/sqlite-database.db"); errors.Is(err, os.ErrNotExist) {
		log.Println("Créant le fichier de base de données : sqlite-database.db...")
		// Si le fichier n'existe pas, le créer
		file, err = os.Create("../database/sqlite-database.db")
	} else {
		// Si le fichier existe, alors l'ouvrir
		file, err = os.Open("../database/sqlite-database.db")
	}

	// Gestion d'une erreur
	if err != nil {
		log.Fatal(err.Error())
	}

	// Fermeture du fichier
	file.Close()
	log.Println("sqlite-database.db OK.")

	// Création de la connection entre le serveur et le fichier de base de données
	sqliteDatabase, _ := sql.Open("sqlite3", "../database/sqlite-database.db")

	// Création de la table
	createTable(sqliteDatabase)

	return sqliteDatabase
}

// Fonction de création de la table des enregistrements dans la base de donnée
func createTable(db *sql.DB) {
	// Commande SQL de création de la table
	// SI ELLE N'EXISTE PAS
	SQLcommand := `CREATE TABLE IF NOT EXISTS records (
		"idRecord" integer NOT NULL PRIMARY KEY AUTOINCREMENT,
		"lat" FLOAT,
		"lon" FLOAT,
		"ville" TEXT,
		"image" TEXT
		);`

	log.Println("Création de la table...")
	// Préparation de la commande SQL
	statement, err := db.Prepare(SQLcommand)
	if err != nil {
		log.Fatal(err.Error())
	}
	// Exécution de la commande SQL
	statement.Exec()
	log.Println("Table OK.")
}

// Fonction d'ajout d'enregistrement dans la BDD
//
// Arguments:
//
//	db : rédérence à l'objet BDD dans le serveur
//	record: référence à un objet Record représentant l'enregistrement
func insertRecord(db *sql.DB, record *Record) {
	// Création de la commande SQL avec 4 paramètres (?) que sont lon, lat, ville, image
	insertRecordSQL := `INSERT INTO records(lon, lat, ville, image) VALUES (?,?,?,?)`
	// Préparation de la commande SQL
	statement, err := db.Prepare(insertRecordSQL)
	if err != nil {
		log.Fatal(err.Error())
	}
	// Execution de le commande SQL avec les éléments de l'enregistrement en paramètre
	_, err = statement.Exec(record.Lon, record.Lat, record.Ville, record.Image)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Println("Enregistrement ajouté.")
}

// Fonction de suppression d'enregistrement dans la BDD
//
// Arguments:
//
//	db : rédérence à l'objet BDD dans le serveur
//	id : identifiant unique de l'enregistrement à supprimer (Nombre entier)
func deleteRecord(db *sql.DB, id int) {
	// Commande SQL de suppression avec 1 paramètre (?)
	deleteRecordSQL := `DELETE FROM records WHERE idRecord = ?`
	// Préparation de la commande SQL
	statement, err := db.Prepare(deleteRecordSQL)
	if err != nil {
		log.Fatal(err.Error())
	}
	// Execution de la commande.
	_, err = statement.Exec(id)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Println("Enregistrement supprimé.")
}

// Fonction de récupération de l'ensemble des enregistrement
//
// Arguments:
//
//	db : rédérence à l'objet BDD dans le serveur
//
// Valeurs de retour:
//
//	Liste de tous les objets Record correspondant aux enregistrement de la BDD
func getAllRecords(db *sql.DB) []Record {
	// Commande SQL de récupération de tous les enregistrement
	queryRecordsSQL := `SELECT idRecord, lat, lon, ville, image FROM records ORDER BY idRecord`
	// Préparation de la commande
	statement, err := db.Prepare(queryRecordsSQL)
	if err != nil {
		log.Fatal(err.Error())
	}
	// Réalisation de la requête et récupération des lignes
	rows, err := statement.Query()
	// Création d'une liste vide d'objet Record
	records := []Record{}
	// Bouclage sur l'ensemble des lignes requêtées
	for rows.Next() {
		// Déclaration de la variable record
		var r Record
		// Peuplement des variables du Record avec les éléments de la ligne de la BDD
		err = rows.Scan(&r.Id, &r.Lat, &r.Lon, &r.Ville, &r.Image)
		if err != nil {
			log.Fatal(err.Error())
		}
		// Ajout à la liste des Record
		records = append(records, r)
	}

	return records
}
