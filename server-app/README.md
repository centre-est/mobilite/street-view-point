# Serveur d'enregistrement des points Google Street View

Serveur réalisé en Go pour l'enregistrement des points enregistrés via l'extension Chrome.

## Utilisation simple (Windows uniquement)

Lancement via `SimpleSVServer.exe`

## Développement et compilation

### Pré-requis

- Go version > 1.17

### Compilation

`go build`