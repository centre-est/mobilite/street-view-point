package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

// Structure de l'objet Handshake permettant l'annonce des applications se connectant au serveur
type Handshake struct {
	Handshake string `json:"handshake"`
}

// variable permettant la transformation d'un protocole HTTP en un protocole websocket
//
// Définition des tailles des buffers pour les messages : 1024bits
var upgrader = websocket.Upgrader{
	// Taille du buffer de lecture
	ReadBufferSize: 1024,
	// Taille du buffer d'écriture
	WriteBufferSize: 1024,
}

// Fonction représentant le point d'entrée d'une première connection websocket
//
// Arguments :
//
//	w: Objet d'écriture de la réponse
//	r: Objet requête HTML
//	wrapper: Structure gérant l'ensemble des objets et leurs communications
func wsEndpoint(w http.ResponseWriter, r *http.Request, wrapper *Wrapper) {
	// Toute origine est acceptée (pas récurisé si appli EN LIGNE)
	upgrader.CheckOrigin = func(r *http.Request) bool {
		return true
	}

	// Augmentation de la connection vers un protocole websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
	}

	// Notification qu'une application essaye de se connecter
	log.Println("Une appli essaie de se connecter. En attente de validation")
	// Attente du message Handshake
	hsType := waitForHandshake(ws)

	// Si l'application qui essaie de se connecter est une extension Chrome et qu'aucune n'est déjà connectée
	// INTERDICTION MULTIPLE CHROME EXTENSION
	if hsType.Handshake == "chrome-ext-hs" && wrapper.ChromeExt == nil {
		// Acceptation de la connection et récupération de l'objet connection dans le Wrapper
		log.Println("Extension chrome acceptée")
		wrapper.ChromeExt = ws

		// Envoi d'un message de bienvenue au client
		err = ws.WriteMessage(1, []byte("Hi Client!"))
		if err != nil {
			log.Println(err)
		}

		// Lancement de la fonction de lecture des communications Chrome en parallèle
		go readerChrome(wrapper)
	}

	// Si l'application qui essaie de se connecter est une interface admin et qu'aucune n'est déjà connectée
	// INTERDICTION MULTIPLE INTERFACE ADMIN
	if hsType.Handshake == "admin-hs" && wrapper.AdminApp == nil {
		// Acceptation de la connection et récupération de l'objet connection dans le Wrapper
		log.Println("Appli admin acceptée")
		wrapper.AdminApp = ws

		// Récupération de l'ensemble des éléments de la base de donnée
		records := getAllRecords(wrapper.Database)
		// S'il y a au moins un enregistrement
		if len(records) != 0 {
			// Encodage du message au format JSON
			message, err := json.Marshal(records)
			if err != nil {
				log.Println(err)
			}

			// Ecriture du message sur la connection à l'interface admin
			err = ws.WriteMessage(1, message)
			if err != nil {
				log.Println(err)
			}
		}

		// Lancement de la fonction de lecture des communications avec l'interface admin en parallèle
		go readerAdmin(wrapper)
	}

}

// Fonction permettant la lecture des messages depuis l'extension Chrome
//
// Arguments:
//
//	wrapper: référence au Wrapper
func readerChrome(wrapper *Wrapper) {
	// Lecture des messages sans fin (tant que la communication est ouverte)
	for {
		// Lecture du message
		_, p, err := wrapper.ChromeExt.ReadMessage()
		if err != nil {
			log.Println(err)
			wrapper.ChromeExt = nil
			return
		}

		// Ecriture dans le channel asynchrone des communications du Wrapper qui se chargera de le traiter
		// en spécifiant l'orgine
		wrapper.Communications <- Communication{Origin: wrapper.ChromeExt, Message: p}
	}
}

// Fonction permettant la lecture des messages depuis l'interface admin
//
// Arguments:
//
//	wrapper: référence au Wrapper
func readerAdmin(wrapper *Wrapper) {
	// Lecture des messages sans fin (tant que la communication est ouverte)
	for {
		// Lecture du message
		_, p, err := wrapper.AdminApp.ReadMessage()
		if err != nil {
			log.Println(err)
			wrapper.AdminApp = nil
			return
		}

		// Ecriture dans le channel asynchrone des communications du Wrapper qui se chargera de le traiter
		// en spécifiant l'orgine
		wrapper.Communications <- Communication{Origin: wrapper.AdminApp, Message: p}
	}
}

// Fonction permettant l'attente d'un Handshake
//
// Arguments
//
//	conn: référence à la connection websocket entrante
func waitForHandshake(conn *websocket.Conn) *Handshake {
	// Attente du message et enregistrement dans la variable p
	_, p, err := conn.ReadMessage()
	if err != nil {
		log.Println(err)
	}
	// création d'un objet Handshake vide
	hsType := new(Handshake)
	// Récupération du type de handshake (i.e. origine)
	json.Unmarshal(p, hsType)

	return hsType
}
