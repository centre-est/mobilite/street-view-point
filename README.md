# Ensemble d'outils pour requête Street View

<!-- vscode-markdown-toc -->
* 1. [Préambule](#Prambule)
* 2. [Fonctionnement](#Fonctionnement)
	* 2.1. [Base de données des points](#Basededonnesdespoints)
	* 2.2. [Communication entre les briques du projet](#Communicationentrelesbriquesduprojet)
* 3. [Composition du dépôt](#Compositiondudpt)
	* 3.1. [Extension Chrome (`chrome-extension`)](#ExtensionChromechrome-extension)
		* 3.1.1. [Installation](#Installation)
		* 3.1.2. [Utilisation](#Utilisation)
	* 3.2. [Serveur](#Serveur)
		* 3.2.1. [Utilisation](#Utilisation-1)
		* 3.2.2. [Compilation](#Compilation)
	* 3.3. [Application de visualisation](#Applicationdevisualisation)
		* 3.3.1. [Installation](#Installation-1)
		* 3.3.2. [Utilisation](#Utilisation-1)
* 4. [Informations du projet](#Informationsduprojet)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->



##  1. <a name='Prambule'></a>Préambule

Dans les études d'accidentologie (*secteur d'activité C5 du Cerema*), l'analyse des procès-verbaux d'accident fait apparaître une notion de sitation géographique en agglomération ou hors-agglomération. Cependant, cette notion n'est pas similaire aux contours adminisitratifs des communes que l'on peut trouver sur OpenStreetMap ou la BDTOPO de l'IGN. Une solution serait donc de disposer de l'ensemble des sections de route en agglomération et hors-agglomération pour faciliter les analyses statistiques via seulement une position géographique.

La solution retenue consiste à délimiter les sections en agglomération des sections hors-agglomérations selon si ces sections se trouvent entre deux panneaux d'agglomération de même nom ou pas.

Face au manque d'exhaustivité des points de panneau d'entrée d'agglomération sur OpenStreetMap [[1](https://wiki.openstreetmap.org/wiki/FR:Tag:traffic_sign%3Dcity_limit)], le Cerema Centre-Est a developpé un moyen de récupérer les emplacements de ces panneaux en navigant dans Street View de Google Maps.

La méthode de définition de la caractéristique en/hors agglomération d'un tronçon de route n'est pas l'objet de ce dépôt.

##  2. <a name='Fonctionnement'></a>Fonctionnement

Le projet est composé de plusieurs briques.
- Une extension Google Chrome permettant à un opérateur d'enregistrer un panneaux d'entrée d'agglomération à partir de sa navigation dans Street View de Google Maps. 
- Une base de donnée SQLlite permettant l'enregistrement des points.
- Une interface permettant de visualiser l'ensemble des points présents dans la base de données.
- Un serveur assurant la communication entre toutes les briques.

Pour le fonctionnement détaillé de chaque brique, voir la partie [composition du dépôt](#3-composition-du-dépôt).


###  2.1. <a name='Basededonnesdespoints'></a>Base de données des points

Le point central de l'application la base de données des points au format SQLlite. Le projet repose sur l'interface entre SQLlite et Go via le module module Go (https://github.com/mattn/go-sqlite3). 

La base de donnée est automatiquement crée dans le fichier `database/sqlite-database.db` si aucun fichier du même nom n'est déjà présent dans le dossier `database`.

La base de donnée possède la structure suivante :

| Nom de la colonne         | Type de variable | Description |
|--------------|:-----:|:-----:|
| idRecord |  `int` | Identifiant du point (clé primaire)
| lat      |  `float` | Latitude
| lon      |  `float` | Longitude
| ville      |  `text` | Nom de la ville
| image      |  `text` | Données de la capture d'écran encodée en 64bits

###  2.2. <a name='Communicationentrelesbriquesduprojet'></a>Communication entre les briques du projet

La communication entre les briques du projet est réalisée via le protocole websocket largement utilisé. Pour la partie serveur le websocket est pris en compte via une augmentation du protocole à l'aide du module Go websocket (https://github.com/gorilla/websocket).

Le chef d'orchestre de toutes les communications est le serveur (`server-app`). Aussi, il n'est pas possible de connecter plusieurs extensions chrome et/ou plusieurs interface de visualisation sur le serveur. L'élement `Wrapper` définit dans le `server-app/wrapper.go` permet cette fonctionnalité en assurant l'unicité de chaque connexion. 

Les paquets transitant sont les suivants :

| Description        | Emetteur | Recepteur |
|--------------|:-----:|:-----:|
| *Handshake* demande de  connexion de l'extension chrome |  Extension Chrome | Serveur | 
|  *Handshake* demande de connexion de l'interface de visualisation     |  Interface de visualisation | Serveur |
| Demande de suppression de point |  Interface de visualisation | Serveur
| Demande d'ajout de point |  Extension Chrome | Serveur
| Envoie de tous les points      |  Serveur | Interface de visualisation



##  3. <a name='Compositiondudpt'></a>Composition du dépôt

Le dépot est composé de 4 dossiers distincts :
- `chrome-extension` composé du code source Javascript de l'extension Chrome
- `server-app` composé du code source Go du serveur ainsi qu'une version compilée en `.exe` pour windows.
- `front-verif` composé du code source de l'application React d'interface de visualisation de tous les points
- `database` dossier vide destiné à recevoir la base de donnée SQLlite `sqlite-database.db` des points

###  3.1. <a name='ExtensionChromechrome-extension'></a>Extension Chrome (`chrome-extension`)

Il s'agit d'une extension de Google Chrome respectant l'architecture classique d'une extension.

####  3.1.1. <a name='Installation'></a>Installation

1. Ouvrir Google Chrome
2. Rentrer l'URL : `chrome://extensions`
3. Activer le mode développeur
4. Cliquer sur le bouton **Charger l'extension non empaquetée**
5. Naviguer jusqu'au dossier `chrome-extensions`

*Il y aura une erreur déclarée dans l'affichage de l'extension dans l'UI de **Toutes les extensions**. Il s'agit d'un problème d'un élément déprécié dans le `manifest.json` sans incidence sur le fonctionnement.*

####  3.1.2. <a name='Utilisation'></a>Utilisation

*Conseil : Epingler l'extension*

1. Cliquer sur l'icône de l'extension

*Si le serveur n'est pas actif, alors l'extension le dit et aucune action est possible. Si le serveur est actif :*

2. Naviguer dans Google Maps en **google.fr (IMPORTANT)** et rentrer dans une vue Street View
   
3. Cliquer sur l'icône de l'extension
4. Entrer le nom de la ville
5. Cliquer sur **Enregistrer le point**

*Si cela fonctionne, alors la cpature d'écran s'affiche en miniature sous le bouton à la place de **No info***

###  3.2. <a name='Serveur'></a>Serveur

####  3.2.1. <a name='Utilisation-1'></a>Utilisation

- Windows :

Lancer `SimpleSVServer.exe`

- Autre OS:

Lancer l'application compilée (**voir compilation**)

####  3.2.2. <a name='Compilation'></a>Compilation

- Prérequis : [Go](https://go.dev/doc/install) version > 1.17 correctement installé et pouvant être appelé via une invite de commande / terminal. Pour vérifier : `go version`. 
- Procédure :
  1. Naviguer dans le dossier `server-app`
  2. Lancer la commande de compilation `go build`

###  3.3. <a name='Applicationdevisualisation'></a>Application de visualisation

####  3.3.1. <a name='Installation-1'></a>Installation

- Prérequis : [Node JS](https://nodejs.org/en)
- Procédure :
  1. Naviguer dans le dossier `front-verif`
  2. Lancer la commande d'installation des dépendances `npm install`

####  3.3.2. <a name='Utilisation-1'></a>Utilisation

- Lancement :
    1. Naviguer dans le dossier `front-verif`
    2. Lancer la commande de lancement de l'application `npm run start`

- Utilisation de l'application : (*Si le serveur est lancé en parallèle*) Tous les points de la base de donnée sont affiché sur la carte et interactifs

##  4. <a name='Informationsduprojet'></a>Informations du projet

- Auteur : Aurélien Clairais - Cerema Centre-Est (*anciennement*)
- Service : CEREMA/DTerCE/DMOB/ESD
- Contact actuel du projet : [Victor Boulanger - Cerema Centre Est](mailto://victor.boulanger@cerema.fr)
- Date de la version : 13/02/2024
